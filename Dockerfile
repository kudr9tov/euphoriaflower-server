FROM openjdk:11

WORKDIR /opt/euphoriaflower

VOLUME /opt/euphoriaflower/apps

ADD ./build/libs/euphoriaflower-0.0.1-SNAPSHOT.jar /opt/euphoriaflower/euphoriaflower.jar

EXPOSE 8081 8000
CMD ["java", "-Xdebug", "-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8000", "-jar", "euphoriaflower.jar"]
