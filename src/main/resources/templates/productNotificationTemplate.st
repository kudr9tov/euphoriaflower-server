<!DOCTYPE html>

<html lang="ru" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0;">
    <meta name="format-detection" content="telephone=no" />

    <style>
        body {
            margin: 0;
            padding: 0;
            min-width: 100%;
            width: 100% !important;
            height: 100% !important;
        }

        table {
          border-collapse: separate;
          border-spacing: 15px;
        }

        img {
            border: 0;
            line-height: 100%;
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
        }

        a,
        a:hover {
            color: #127DB3;
        }
    </style>
    <title>"ГлавКом" Новый запрос</title>

</head>

<body style="width: 100%;text-align: center;font-family: Oswald">
    <!--[if (gte mso 9)|(IE)]><table style="text-align:center; width: 620px;border: 1px solid #000000;
        "><tr><td><![endif]-->
    <!--[if !mso]><!-- --><div style="width: 100%;background-color: #fff; text-align: center;margin: 5px auto;">
        <!--<![endif]-->
        <div>
            <div style="width:100%;overflow:hidden;display: inline-table;">
                <!--[if (gte mso 9)|(IE)]><img src="cid:{{logoCid}}" style="width: 100%;" /><![endif]-->
                <!--[if !mso]><!-- --><img src="https://gktorg.ru/images/glavcom-logo-V2.png" style="height:65px;display: inline-block;" alt="ГлавКом" /><!--<![endif]-->
                <div style="font-size: 40px;display: inline-block;line-height: 55px;vertical-align: top;color: #a9a9a9;border-bottom: 1px solid;font-family: Oswald">ГлавКом</div>
            </div>
        </div>
        <div>
            <div style="width:100%;overflow:hidden;display: inline-table;padding-bottom: 10px;color: #75528C;">
                <!--[if (gte mso 9)|(IE)]><img src="cid:{{logoCid}}" style="width: 100%;" /><![endif]-->
                <!--[if !mso]><!-- --><img src="https://gktorg.ru/images/pYoMj_zajavka.png" style="max-height: 320px;display: inline-block;" alt="Заявка" /><!--<![endif]-->
            </div>
        </div>

	   <div style="font-family: 'Alumni Sans', sans-serif;width:100%;box-sizing:border-box;letter-spacing: 0.2px;font-size:28px;padding:10px 0;font-weight: bold;text-align:center;display: inline-table;">
        $action$
        </div>
        $admin$


        <div style="width:100%;text-align:center;margin:10px 0;">
            $clientInfo$
            <table style="width: 80%; margin: 0 auto;"></div>
                $productsHeader$
                $products$
            </table>

        </div>
        <!--[if !mso]><!-- -->
    </div><!--<![endif]-->
    <!--[if (gte mso 9)|(IE)]> </td></tr> </table> <![endif]-->
</body>
</html>