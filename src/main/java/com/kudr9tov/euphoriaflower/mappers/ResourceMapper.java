package com.kudr9tov.euphoriaflower.mappers;

import com.kudr9tov.euphoriaflower.entity.Picture;
import com.kudr9tov.euphoriaflower.entity.Resource;

import java.util.function.Function;

public final class ResourceMapper {
    private ResourceMapper() {
    }
    public static Function<Resource, Picture> resourceToPicture = resource -> {
        Picture picture = new Picture();
        picture.setId(resource.getId());
        picture.setUrl(resource.getResourceUrl());
        try {
            picture.setWidth((Integer) resource.getResourceParameters().get("width"));
            picture.setHeight((Integer) resource.getResourceParameters().get("height"));
        } catch (Exception ignored) {
        }
        return picture;
    };
}
