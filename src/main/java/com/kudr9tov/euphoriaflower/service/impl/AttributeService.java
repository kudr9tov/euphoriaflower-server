package com.kudr9tov.euphoriaflower.service.impl;

import com.kudr9tov.euphoriaflower.entity.Attribute;
import com.kudr9tov.euphoriaflower.entity.AttributeValue;
import com.kudr9tov.euphoriaflower.entity.AttributeValues;
import com.kudr9tov.euphoriaflower.entity.Page;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.persistence.impl.AttributeDAO;
import com.kudr9tov.euphoriaflower.persistence.impl.AttributeValueDAO;
import com.kudr9tov.euphoriaflower.service.InterfaceService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Service
public class AttributeService implements InterfaceService<Attribute> {

    private final AttributeDAO dao;
    private final AttributeValueDAO attributeValueDAO;

    @Autowired
    public AttributeService(final AttributeDAO dao,
                            final AttributeValueDAO attributeValueDAO) {
        this.dao = dao;
        this.attributeValueDAO = attributeValueDAO;
    }

    @Override
    public Page<Attribute> getAll(int offset, int limit) {
        return dao.getAll(offset, limit);
    }

    @Override
    public Attribute get(Integer id) {
        return dao.get(id);
    }

    @Override
    public Attribute create(Attribute attribute) {
        return dao.create(attribute);
    }

    @Override
    public Attribute update(Attribute attribute) {
        return dao.update(attribute);
    }

    @Override
    public void delete(Integer id) {
        if (BooleanUtils.isFalse(dao.isExist(id))) {
            throw new NotFoundException(String.format("Attribute with id `%d` not found", id));
        }
        dao.delete(id);
    }

    public AttributeValue addAttributeValue(AttributeValue attributeValue) {
        if (BooleanUtils.isFalse(dao.isExist(attributeValue.getId()))) {
            throw new NotFoundException(String.format("Attribute with id `%d` not found", attributeValue.getId()));
        }
        return attributeValueDAO.addAttributeValue(attributeValue);
    }

    public List<AttributeValues> getAttributeValues(Collection<Attribute> attributes) {
        if (CollectionUtils.isEmpty(attributes)) {
            return Collections.emptyList();
        }
        return attributeValueDAO.getAttributeValuesMap(attributes);
    }
}
