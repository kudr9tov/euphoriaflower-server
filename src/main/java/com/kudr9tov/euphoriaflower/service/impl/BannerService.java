package com.kudr9tov.euphoriaflower.service.impl;

import com.kudr9tov.euphoriaflower.entity.Banner;
import com.kudr9tov.euphoriaflower.entity.Page;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.persistence.impl.BannerDAO;
import com.kudr9tov.euphoriaflower.service.InterfaceService;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BannerService implements InterfaceService<Banner> {

    private final BannerDAO dao;

    @Autowired
    public BannerService(BannerDAO dao) {
        this.dao = dao;
    }

    @Override
    public Page<Banner> getAll(int page, int limit) {
        return dao.getAll(page, limit);
    }

    @Override
    public Banner get(Integer id) {
        return dao.get(id);
    }

    @Override
    public Banner create(Banner banner) {
        return dao.create(banner);
    }

    @Override
    public Banner update(Banner banner) {
        return dao.update(banner);
    }

    @Override
    public void delete(Integer id) {
        if (BooleanUtils.isFalse(dao.isExist(id))) {
            throw new NotFoundException(String.format("Banner with id `%d` not found", id));
        }
        dao.delete(id);
    }
}
