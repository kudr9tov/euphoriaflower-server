package com.kudr9tov.euphoriaflower.service.impl;

import com.kudr9tov.euphoriaflower.entity.AttributeValue;
import com.kudr9tov.euphoriaflower.entity.Category;
import com.kudr9tov.euphoriaflower.entity.Page;
import com.kudr9tov.euphoriaflower.entity.Picture;
import com.kudr9tov.euphoriaflower.entity.Product;
import com.kudr9tov.euphoriaflower.entity.Resource;
import com.kudr9tov.euphoriaflower.entity.enums.EntityType;
import com.kudr9tov.euphoriaflower.exception.AlreadyExistException;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.persistence.impl.ProductDAO;
import com.kudr9tov.euphoriaflower.service.InterfaceService;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductService implements InterfaceService<Product> {

    private final ProductDAO dao;
    private final ResourceService resourceService;
    private final CategoryService categoryService;
    private final AttributeService attributeService;

    @Autowired
    public ProductService(final ProductDAO dao,
                          final ResourceService resourceService,
                          final CategoryService categoryService,
                          final AttributeService attributeService) {
        this.dao = dao;
        this.resourceService = resourceService;
        this.categoryService = categoryService;
        this.attributeService = attributeService;
    }

    @Override
    public Page<Product> getAll(int page, int limit) {
        return dao.getAll(page, limit);
    }

    @Override
    public Product get(Integer id) {
        return null;
    }

    @Override
    @Transactional
    public Product create(Product productToCreate) {
        if (dao.checkSlug(productToCreate.getSlug())) {
            throw new AlreadyExistException(String.format("Slug `%s` already used for another product", productToCreate.getSlug()));
        }
        Product product = dao.create(productToCreate);
        processOtherProductEntities(productToCreate, product);
        return product;
    }

    private void processOtherProductEntities(Product productToCreate, Product product) {
        for (Picture picture : productToCreate.getPictures()) {
            Resource resource = resourceService.addResourceToEntity(picture.getId(), product.getId(), EntityType.PRODUCT);
            picture.setUrl(resource.getResourceUrl());
            try {
                picture.setWidth((Integer) resource.getResourceParameters().get("width"));
                picture.setHeight((Integer) resource.getResourceParameters().get("height"));
            } catch (Exception ignored) {
            }
            product.addPicture(picture);
        }

        for (Category category : productToCreate.getCategories()) {
            categoryService.addCategoryToProduct(category.getId(), product.getId());
            product.addCategory(category);
        }

        for (AttributeValue attribute : productToCreate.getAttributes()) {
            attribute.setProductId(product.getId());
            product.addAttribute(attributeService.addAttributeValue(attribute));
        }
    }

    @Override
    public Product update(Product productToUpdate) {
        if (dao.isExist(productToUpdate.getId())) {
            throw new AlreadyExistException(String.format("Product `%s` not found", productToUpdate.getId()));
        }
        Product product = dao.update(productToUpdate);
        resourceService.clearEntityResources(product.getId(), EntityType.PRODUCT);
        categoryService.clearProductCategory(product.getId());

        processOtherProductEntities(productToUpdate, product);

        return product;
    }

    @Override
    public void delete(Integer id) {
        if (BooleanUtils.isFalse(dao.isExist(id))) {
            throw new NotFoundException(String.format("Product with id `%d` not found", id));
        }
        dao.delete(id);
    }

    public Product getProduct(String slug) {
        return dao.getProduct(slug);
    }

}
