package com.kudr9tov.euphoriaflower.service.impl;

import com.kudr9tov.euphoriaflower.entity.Category;
import com.kudr9tov.euphoriaflower.entity.Page;
import com.kudr9tov.euphoriaflower.exception.AlreadyExistException;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.persistence.impl.CategoryDAO;
import com.kudr9tov.euphoriaflower.service.InterfaceService;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryService implements InterfaceService<Category> {

    private final CategoryDAO dao;

    @Autowired
    public CategoryService(CategoryDAO dao) {
        this.dao = dao;
    }

    @Override
    public Page<Category> getAll(int page, int limit) {
        return dao.getAll(page, limit);
    }

    @Override
    public Category get(Integer id) {
        return null;
    }

    @Override
    public Category create(Category category) {
        if (dao.checkSlug(category.getSlug())) {
            throw new AlreadyExistException(String.format("Slug `%s` already used for another category", category.getSlug()));
        }
        return dao.create(category);
    }

    @Override
    public Category update(Category category) {
        return dao.update(category);
    }

    @Override
    public void delete(Integer id) {
        if (BooleanUtils.isFalse(dao.isExist(id))) {
            throw new NotFoundException(String.format("Category with id `%d` not found", id));
        }
        dao.delete(id);
    }

    public List<Category> getProductCategories(Integer productId) {
        return dao.getCategoriesByProduct(productId);
    }

    public Category getCategoryBySlug(String slug) {
        return dao.getCategoryBySlug(slug);
    }

    public Category getCategory(String slug) {
        return getCategoryBySlug(slug);
    }

    public void clearProductCategory(Integer productId) {
        dao.deleteProductCategories(productId);
    }

    public void addCategoryToProduct(Integer categoryId, Integer productId) {
        if (BooleanUtils.isFalse(dao.isExist(categoryId))) {
            throw new NotFoundException(String.format("Category with id `%d` not found", categoryId));
        }
        dao.addCategoryToProduct(categoryId, productId);
    }
}
