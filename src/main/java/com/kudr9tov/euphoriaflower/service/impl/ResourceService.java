package com.kudr9tov.euphoriaflower.service.impl;

import com.kudr9tov.euphoriaflower.entity.Page;
import com.kudr9tov.euphoriaflower.entity.Resource;
import com.kudr9tov.euphoriaflower.entity.enums.EntityType;
import com.kudr9tov.euphoriaflower.entity.enums.FileType;
import com.kudr9tov.euphoriaflower.entity.enums.ResourceType;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.persistence.impl.ResourceDAO;
import com.kudr9tov.euphoriaflower.service.InterfaceService;
import com.kudr9tov.euphoriaflower.utils.model.SavedFile;
import com.kudr9tov.euphoriaflower.utils.shared.UploadFileService;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Service
public class ResourceService implements InterfaceService<Resource> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceService.class);

    private final ResourceDAO dao;
    private final UploadFileService uploadFileService;

    @Autowired
    public ResourceService(final ResourceDAO dao,
                           final UploadFileService uploadFileService) {
        this.dao = dao;
        this.uploadFileService = uploadFileService;
    }

    @Override
    public Page<Resource> getAll(int page, int limit) {
        return dao.getAll(page, limit);
    }

    public Resource get(Integer id) {
        return dao.get(id);
    }

    @Override
    public Resource create(Resource resource) {
        return dao.create(resource);
    }

    @Override
    public Resource update(Resource resource) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Integer id) {
        if (BooleanUtils.isFalse(dao.isExist(id))) {
            throw new NotFoundException(String.format("Resource with id `%d` not found", id));
        }
        dao.delete(id);
    }

    public Resource createResource(MultipartFile file) {

        String originalFilename = file.getOriginalFilename();
        FileType type = EnumUtils.getEnumIgnoreCase(FileType.class, FilenameUtils.getExtension(file.getOriginalFilename()));
        SavedFile savedFile = uploadFileService.saveUploadedFiles(file);

        try {
            Resource resource = new Resource();
            resource.setResourceUrl(savedFile.getDownloadUrl());
            resource.setType(type.getResourceType());
            resource.addResourceParameter("originalName", originalFilename);
            resource.addResourceParameter("size", file.getSize());
            resource.addResourceParameter("type", type.name().toLowerCase());
            if (ResourceType.PICTURE.equals(type.getResourceType())) {
                BufferedImage read = ImageIO.read(savedFile.getPath().toFile());
                resource.addResourceParameter("width", read.getWidth());
                resource.addResourceParameter("height", read.getHeight());
            }
            return create(resource);
        } catch (IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new IllegalStateException("Can't upload file", e);
        }
    }


    public File getResource(String file) {
        return uploadFileService.getFile(file);
    }

    public Resource addResourceToEntity(Integer resourceId, Integer entityId, EntityType entityType) {
        if (BooleanUtils.isFalse(dao.isExist(resourceId))) {
            throw new NotFoundException(String.format("Resource with id `%d` not found", resourceId));
        }
        return dao.addResourceToEntity(resourceId, entityId, entityType);
    }

    public void clearEntityResources(Integer entityId, EntityType entityType) {
        dao.deleteEntityResources(entityId, entityType);
    }
}
