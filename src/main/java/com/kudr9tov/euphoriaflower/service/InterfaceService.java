package com.kudr9tov.euphoriaflower.service;

import com.kudr9tov.euphoriaflower.entity.Page;

public interface InterfaceService<E> {
    Page<E> getAll(int page, int limit);

    E get(Integer id);

    E create(E entity);

    E update(E entity);

    void delete(Integer id);
}
