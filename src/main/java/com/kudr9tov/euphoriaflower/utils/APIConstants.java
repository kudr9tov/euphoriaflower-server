package com.kudr9tov.euphoriaflower.utils;

public final class APIConstants {

    private APIConstants() {
    }

    public static final String API_ROOT = "/api";
    public static final String ATTRIBUTE = APIConstants.API_ROOT + "/attribute";
    public static final String CATEGORY = APIConstants.API_ROOT + "/category";
    public static final String PRODUCT = APIConstants.API_ROOT + "/product";
    public static final String BANNER = APIConstants.API_ROOT + "/banner";
    public static final String RESOURCE = APIConstants.API_ROOT + "/resource";

}
