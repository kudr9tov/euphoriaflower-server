package com.kudr9tov.euphoriaflower.utils.shared;

import com.kudr9tov.euphoriaflower.entity.Resource;
import com.kudr9tov.euphoriaflower.exception.FileUploadException;
import com.kudr9tov.euphoriaflower.utils.model.SavedFile;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.stream.Stream;

@Service
public class UploadFileService {

    private final String uploadFolder;
    private final String downloadUrl;

    public UploadFileService(@Value("${sem.platform.admin.upload.folder}") String uploadFolder,
                             @Value("${sem.platform.admin.download.url}") String downloadUrl) {
        this.uploadFolder = uploadFolder;
        this.downloadUrl = downloadUrl;
        File directory = new File(uploadFolder);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }

    public SavedFile saveUploadedFiles(MultipartFile file) {
        try {
            String uploadedFileName = RandomStringUtils.randomAlphanumeric(5) + "." + FilenameUtils.getExtension(file.getOriginalFilename());
            final String safeFileName = getSafeFileName(uploadedFileName);
            Path path = Paths.get(uploadFolder + safeFileName);
            Files.copy(file.getInputStream(), path);
            return new SavedFile(path.toAbsolutePath().toString(), String.format("%s%s", downloadUrl, safeFileName), path);
        } catch (IOException e) {
            throw new FileUploadException(e);
        }
    }

    private String getSafeFileName(String fileName) {
        return fileName.replaceAll("[%#?]", StringUtils.EMPTY);
    }

    public File getFile(String file) {
        File directory = new File(uploadFolder);
        if(ArrayUtils.isNotEmpty(directory.listFiles())){
            return Stream.of(Objects.requireNonNull(directory.listFiles()))
                    .filter(dirFile -> dirFile.getName().equals(file))
                    .findFirst()
                    .orElseThrow();
        }
        throw new NoSuchElementException("No value present");
    }
}
