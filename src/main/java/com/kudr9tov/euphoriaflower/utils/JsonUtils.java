package com.kudr9tov.euphoriaflower.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public final class JsonUtils {
    private static final Logger LOGGER = LoggerFactory.getLogger(JsonUtils.class);

    private static JsonUtils instance;
    public static final ObjectMapper MAPPER = new ObjectMapper();

    private JsonUtils() {
    }

    public String objectToJson(Object o) {
        return objectToJson(o, null);
    }

    public String objectToJson(Object o, String defaultValue) {
        if (Objects.isNull(o)) {
            return defaultValue;
        }
        try {
            return MAPPER.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            LOGGER.warn("Doesn't parse validation error", e);
        }
        return defaultValue;
    }

    public ObjectNode createObjectNode() {
        return MAPPER.createObjectNode();
    }

    public ArrayNode createArrayNode(Collection<String> collection) {
        ArrayNode arrayNode = MAPPER.createArrayNode();
        if (CollectionUtils.isNotEmpty(collection)) {
            for (String item : collection) {
                arrayNode.add(item);
            }
        }
        return arrayNode;
    }

    public String createJsonToString(String itemName, String value) {
        return MAPPER.createObjectNode().put(itemName, value).toString();
    }

    public String createJsonToString(String itemName, Double value) {
        return MAPPER.createObjectNode().put(itemName, value).toString();
    }

    public <T extends Collection, K> Collection<K> jsonArrayToCollection(String arrayJson, Class<T> collection, Class<K> collectionType) throws IOException {
        if (StringUtils.isBlank(arrayJson)) {
            return Collections.emptySet();
        }
        return MAPPER.readValue(arrayJson, MAPPER.getTypeFactory().constructCollectionType(collection, collectionType));
    }

    public static synchronized JsonUtils getInstance() {
        if (instance == null) {
            instance = new JsonUtils();
        }
        return instance;
    }


    public <K, V> Map<K, V> jsonToMap(String value, Class<K> keyType, Class<K> valueType) throws JsonProcessingException {
        return new ObjectMapper().readValue(value, MAPPER.getTypeFactory().constructMapType(HashMap.class, keyType, valueType));
    }

    public Map<String, Object> jsonToMap(String value) throws JsonProcessingException {
        return new ObjectMapper().readValue(value, MAPPER.getTypeFactory().constructMapType(HashMap.class, String.class, Object.class));
    }

    public Integer jsonIntValue(JsonNode node, String fieldName, Integer... defaultValue) {
        JsonNode nodeValue = node.get(fieldName);
        if (Objects.isNull(nodeValue)) {
            return ObjectUtils.firstNonNull(defaultValue);
        }
        return nodeValue.asInt(ObjectUtils.firstNonNull(defaultValue));
    }

    public String jsonTextValue(JsonNode node, String fieldName, String... defaultValue) {
        JsonNode nodeValue = node.get(fieldName);
        if (Objects.isNull(nodeValue)) {
            return ObjectUtils.firstNonNull(defaultValue);
        }
        return nodeValue.asText(ObjectUtils.firstNonNull(defaultValue));
    }
}