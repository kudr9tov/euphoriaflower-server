package com.kudr9tov.euphoriaflower.utils;

import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;

public final class SymbolUtils {
    public static final String DOUBLE_COLON = "::";
    public static final String DOT = ".";
    public static final String COLON = ":";
    public static final String SEMICOLON = ";";
    public static final String COMMA = ",";
    public static final String TAB = "\t";
    public static final String OPEN_PARENTHESIS = "(";
    public static final String CLOSE_PARENTHESIS = ")";
    public static final String EQUAL = "=";
    public static final String AMPERSAND = "&";
    public static final String SINGLE_QUOTE = "'";
    public static final String QUOTE = "\"";
    public static final String PERCENT = "%";
    public static final String HYPHEN = "-";
    public static final String BOTTOM_LINE = "_";
    public static final String PLUS = "+";
    public static final Character STRAIGHT_SLASH = '/';
    public static final Character BACKSLASH = '\\';
    public static final String QUESTION_MARK = "?";
    public static final String SQUARE_BRACKET_START = "[";
    public static final String SQUARE_BRACKET_END = "]";

    private SymbolUtils() {
    }

    public static String replaceSpaceSymbols(String value) {
        String result = RegExUtils.replaceAll(value, "\\u00A0", StringUtils.SPACE);
        result = RegExUtils.replaceAll(result, "\\u200b", StringUtils.SPACE);
        return StringUtils.trimToEmpty(result);
    }

}