package com.kudr9tov.euphoriaflower.utils;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public final class ResponseEntityUtils {

    private ResponseEntityUtils() {
    }

    public static ResponseEntity<Object> responseEntity(final String json) {
        if (StringUtils.isBlank(json)) {
            return new ResponseEntity<>("{}", HttpStatus.OK);
        }
        try {
            JsonNode result = JsonUtils.MAPPER.readValue(json, JsonNode.class);
            return new ResponseEntity<>(result, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
