package com.kudr9tov.euphoriaflower.utils.model;

import java.nio.file.Path;

public class SavedFile {
    private String absolutePath;
    private String downloadUrl;
    private Path path;

    public SavedFile(String absolutePath, String downloadUrl, Path path) {
        this.absolutePath = absolutePath;
        this.downloadUrl = downloadUrl;
        this.path = path;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }
}
