package com.kudr9tov.euphoriaflower.utils.model;

import org.springframework.http.HttpStatus;

import java.util.List;

public class ApiError {
    private final String methodType;
    private final String endpoint;
    private final HttpStatus status;
    private final String message;
    private final List<String> errors;

    public ApiError(String methodType, String endpoint, HttpStatus status, String message, List<String> errors) {
        this.methodType = methodType;
        this.endpoint = endpoint;
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    public String getMethodType() {
        return methodType;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public List<String> getErrors() {
        return errors;
    }
}
