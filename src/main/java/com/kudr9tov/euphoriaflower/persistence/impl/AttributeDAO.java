package com.kudr9tov.euphoriaflower.persistence.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.kudr9tov.euphoriaflower.entity.Attribute;
import com.kudr9tov.euphoriaflower.entity.Page;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.persistence.InterfaceDAO;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Repository
public class AttributeDAO implements InterfaceDAO<Attribute> {
    private static final Logger LOGGER = LoggerFactory.getLogger(AttributeDAO.class);

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public AttributeDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    @Override
    public Attribute get(Integer id) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        return nameJdbcTemplate.query("SELECT id, name FROM attribute WHERE id = :id;", params, mapResult())
                .stream()
                .findFirst()
                .orElseThrow(()-> new NotFoundException(String.format("Attribute with id `%d` not found", id)));
    }

    @Override
    public Page<Attribute> getAll(int page, int limit) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("offset", page > 1 ? page * limit : 0);
        params.put("limit", limit);
        List<Attribute> banners = nameJdbcTemplate.query("SELECT id, name FROM attribute " +
                "OFFSET :offset " +
                "LIMIT :limit", params, mapResult());

        int totalAttributes = elementsCount();

        Page<Attribute> attributePage = new Page<>(page, limit);
        attributePage.setContent(banners);
        attributePage.setTotalElements(totalAttributes);
        attributePage.setPages((int) Math.ceil((double) totalAttributes / limit));
        return attributePage;
    }

    @Override
    public Attribute create(Attribute attribute) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("name", attribute.getName());
        return nameJdbcTemplate.query("INSERT INTO attribute (name) VALUES (:name) RETURNING id, name;", params, mapResult())
                .stream()
                .findFirst()
                .orElseThrow();
    }

    @Override
    public Attribute update(Attribute attribute) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("id", attribute.getName());
        params.put("name", attribute.getName());
        return nameJdbcTemplate.query("UPDATE attribute SET name = :name WHERE id = :id RETURNING id, name;", params, mapResult())
                .stream()
                .findFirst()
                .orElseThrow();
    }

    @Override
    public void delete(Integer id) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        nameJdbcTemplate.update("DELETE FROM attribute WHERE id = :id;", params);
    }

    @Override
    public boolean isExist(Integer id) {
        Boolean isExist = nameJdbcTemplate.queryForObject("SELECT true FROM attribute WHERE id = :id;", ImmutableMap.of("id", id), Boolean.class);
        return ObjectUtils.defaultIfNull(isExist, Boolean.FALSE);
    }

    @Override
    public int elementsCount() {
        Integer count = nameJdbcTemplate.queryForObject("SELECT count(id) FROM attribute ", Collections.emptyMap(), Integer.class);
        return ObjectUtils.defaultIfNull(count, 0);
    }

    private RowMapper<Attribute> mapResult() {
        return (rs, i) -> {
            Attribute attribute = new Attribute();
            attribute.setId(rs.getInt(1));
            attribute.setName(rs.getString(2));
            return attribute;
        };
    }
}
