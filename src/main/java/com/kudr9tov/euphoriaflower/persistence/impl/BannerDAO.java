package com.kudr9tov.euphoriaflower.persistence.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.kudr9tov.euphoriaflower.entity.Banner;
import com.kudr9tov.euphoriaflower.entity.Link;
import com.kudr9tov.euphoriaflower.entity.Page;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.persistence.InterfaceDAO;
import com.kudr9tov.euphoriaflower.utils.JsonUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Repository
public class BannerDAO implements InterfaceDAO<Banner> {
    private static final Logger LOGGER = LoggerFactory.getLogger(BannerDAO.class);

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public BannerDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    @Override
    public Page<Banner> getAll(int page, int limit) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("offset", page > 1 ? page * limit : 0);
        params.put("limit", limit);
        List<Banner> banners = nameJdbcTemplate.query("SELECT id, image, titles, link, step, updated " +
                "FROM carousel " +
                "OFFSET :offset " +
                "LIMIT :limit", params, mapResult());

        int totalBanners = elementsCount();

        Page<Banner> bannerPage = new Page<>(page, limit);
        bannerPage.setContent(banners);
        bannerPage.setTotalElements(totalBanners);
        bannerPage.setPages((int) Math.ceil((double) totalBanners / limit));
        return bannerPage;
    }

    @Override
    public Banner get(Integer id) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        return nameJdbcTemplate.query("SELECT id, image, titles, link, step, updated FROM carousel WHERE id = :id;", params, mapResult())
                .stream()
                .findFirst()
                .orElseThrow(() -> new NotFoundException(String.format("Banner with id `%d` not found", id)));
    }

    @Override
    public Banner create(Banner banner) {
        try {
            Map<String, Object> params = createParameters(banner);
            Integer id = nameJdbcTemplate.queryForObject("INSERT INTO carousel (image, titles, link, step) " +
                    "VALUES (:image, cast(:titles as JSONB), cast(:link as JSONB), :step) " +
                    "RETURNING id;", params, Integer.class);
            banner.setId(id);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        return banner;
    }

    @Override
    public Banner update(Banner banner) {
        try {
            Map<String, Object> params = createParameters(banner);
            List<Banner> result = nameJdbcTemplate.query("UPDATE carousel  " +
                    "SET image  = data.image,  " +
                    "    titles = data.titles,  " +
                    "    link   = data.link,  " +
                    "    step   = data.step  " +
                    "FROM (VALUES (:image, cast(:titles as JSONB), cast(:link as JSONB), :step)) as data (image, titles, link, step) " +
                    "RETURNING carousel.id, carousel.image, carousel.titles, carousel.link, carousel.step;", params, mapResult());
            if (CollectionUtils.isNotEmpty(result)) {
                return result.get(0);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Can't update banner", e);
        }
        throw new IllegalArgumentException("Can't update banner");
    }

    @Override
    public void delete(Integer id) {
        nameJdbcTemplate.update("DELETE FROM carousel WHERE id = :id", ImmutableMap.of("id", id));
    }

    @Override
    public boolean isExist(Integer id) {
        Boolean isExist = nameJdbcTemplate.queryForObject("SELECT exists(SELECT id FROM carousel WHERE id = :id);", ImmutableMap.of("id", id), Boolean.class);
        return ObjectUtils.defaultIfNull(isExist, Boolean.FALSE);
    }

    @Override
    public int elementsCount() {
        Integer count = nameJdbcTemplate.queryForObject("SELECT count(id) FROM carousel ", Collections.emptyMap(), Integer.class);
        return ObjectUtils.defaultIfNull(count, 0);
    }

    private Map<String, Object> createParameters(Banner banner) throws JsonProcessingException {
        Map<String, Object> params = Maps.newHashMap();
        params.put("image", banner.getImageUrl());
        params.put("titles", JsonUtils.MAPPER.writeValueAsString(banner.getTitles()));
        params.put("link", JsonUtils.MAPPER.writeValueAsString(banner.getLink()));
        params.put("step", banner.getStep());
        return params;
    }

    private RowMapper<Banner> mapResult() {
        return (rs, i) -> {
            Banner rBanner = new Banner();

            rBanner.setId(rs.getInt(1));
            rBanner.setImageUrl(rs.getString(2));
            try {
                rBanner.setTitles(JsonUtils.getInstance().jsonToMap(rs.getString(3), String.class, String.class));
            } catch (JsonProcessingException e) {
                LOGGER.warn("Can't parse `titles`");
            }
            try {
                rBanner.setLink(JsonUtils.MAPPER.readValue(rs.getString(4), Link.class));
            } catch (JsonProcessingException e) {
                LOGGER.warn("Can't parse `link`");
            }
            return rBanner;
        };
    }

}
