package com.kudr9tov.euphoriaflower.persistence.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.kudr9tov.euphoriaflower.entity.Category;
import com.kudr9tov.euphoriaflower.entity.Page;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.persistence.InterfaceDAO;
import com.kudr9tov.euphoriaflower.utils.JsonUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Repository
public class CategoryDAO implements InterfaceDAO<Category> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryDAO.class);

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public CategoryDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    @Override
    public Page<Category> getAll(int page, int limit) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("offset", page > 1 ? page * limit : 0);
        params.put("limit", limit);
        List<Category> categories = nameJdbcTemplate.query("SELECT id, name, slug " +
                "FROM category " +
                "OFFSET :offset " +
                "LIMIT :limit", params, mapResult());

        int totalCategories = elementsCount();

        Page<Category> categoryPage = new Page<>(page, limit);
        categoryPage.setContent(categories);
        categoryPage.setTotalElements(totalCategories);
        categoryPage.setPages((int) Math.ceil((double) totalCategories / limit));
        return categoryPage;
    }

    @Override
    public Category get(Integer id) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        return nameJdbcTemplate.query("SELECT id, name, slug " +
                        "FROM category " +
                        "WHERE id = :id;", params, mapResult())
                .stream()
                .findFirst()
                .orElseThrow(() -> new NotFoundException(String.format("Category with id `%d` not found", id)));
    }

    @Override
    public Category create(Category category) {

        try {
            Map<String, Object> params = createParameters(category);
            Integer id = nameJdbcTemplate.queryForObject("INSERT INTO category (name, slug) VALUES (:name, :slug);", params, Integer.class);
            category.setId(id);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }

        return category;
    }

    @Override
    public Category update(Category category) {
        try {
            Map<String, Object> params = createParameters(category);
            List<Category> categories = nameJdbcTemplate.query("UPDATE category " +
                    "  SET name = :name, " +
                    "      slug = :slug " +
                    "WHERE id = :id;", params, mapResult());
            if (CollectionUtils.isNotEmpty(categories)) {
                return categories
                        .stream()
                        .findFirst()
                        .orElseThrow(() -> new IllegalArgumentException("Can't update category"));
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Can't update category", e);
        }
        throw new IllegalArgumentException("Can't update category");
    }

    @Override
    public void delete(Integer id) {
        nameJdbcTemplate.update("DELETE FROM category WHERE id = :id", ImmutableMap.of("id", id));
    }

    @Override
    public boolean isExist(Integer id) {
        Boolean isExist = nameJdbcTemplate.queryForObject("SELECT exists(SELECT id FROM category WHERE id = :id);", ImmutableMap.of("id", id), Boolean.class);
        return ObjectUtils.defaultIfNull(isExist, Boolean.FALSE);
    }

    @Override
    public int elementsCount() {
        Integer count = nameJdbcTemplate.queryForObject("SELECT count(id) FROM category", Collections.emptyMap(), Integer.class);
        return ObjectUtils.defaultIfNull(count, 0);
    }

    public Boolean checkSlug(String slug) {
        return nameJdbcTemplate.queryForObject("SELECT exists(SELECT true FROM category WHERE slug = :slug);", ImmutableMap.of("slug", slug), Boolean.class);
    }

    public Category getCategoryBySlug(String slug) {
        List<Category> categories = nameJdbcTemplate.query("SELECT id, name, slug FROM category WHERE slug = :slug;", ImmutableMap.of("slug", slug), mapResult());
        if (categories.size() > 1) {
            throw new IllegalArgumentException(String.format("Something was wrong where processed result. Expected: 1, Actual: %d", categories.size()));
        }
        return categories
                .stream()
                .findFirst()
                .orElseThrow(() -> new NotFoundException(String.format("Category with slug `%s` not found", categories.size())));
    }

    public List<Category> getCategoriesByProduct(Integer productId) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("productId", productId);
        return nameJdbcTemplate.query("SELECT c.id, c.name, c.slug " +
                "FROM category c " +
                " INNER JOIN category_to_product ctp on c.id = ctp.category_id " +
                "WHERE ctp.product_id = :productId;", params, mapResult());
    }

    public void addCategoryToProduct(Integer categoryId, Integer productId) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("categoryId", categoryId);
        params.put("productId", productId);
        nameJdbcTemplate.update("INSERT INTO category_to_product (category_id, product_id) VALUES (:categoryId, :productId)", params);
    }

    public void removeCategoryFromProduct(Integer categoryId, Integer productId) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("categoryId", categoryId);
        params.put("productId", productId);
        nameJdbcTemplate.update("DELETE FROM category_to_product WHERE category_id = :categoryId AND product_id = :productId)", params);
    }

    public void deleteProductCategories(Integer productId) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("productId", productId);
        nameJdbcTemplate.update("DELETE FROM category_to_product WHERE product_id = :productId)", params);
    }

    private Map<String, Object> createParameters(Category category) throws JsonProcessingException {
        Map<String, Object> params = Maps.newHashMap();
        params.put("name", category.getName());
        params.put("slug", JsonUtils.MAPPER.writeValueAsString(category.getSlug()));
        if (Objects.nonNull(category.getId())) {
            params.put("id", JsonUtils.MAPPER.writeValueAsString(category.getId()));
        }
        return params;
    }

    private RowMapper<Category> mapResult() {
        return (rs, i) -> {
            Category category = new Category();
            category.setId(rs.getInt(1));
            category.setName(rs.getString(2));
            category.setSlug(rs.getString(3));
            return category;
        };
    }
}