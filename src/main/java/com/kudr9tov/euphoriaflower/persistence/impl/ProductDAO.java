package com.kudr9tov.euphoriaflower.persistence.impl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.kudr9tov.euphoriaflower.entity.AttributeValue;
import com.kudr9tov.euphoriaflower.entity.Page;
import com.kudr9tov.euphoriaflower.entity.Picture;
import com.kudr9tov.euphoriaflower.entity.Product;
import com.kudr9tov.euphoriaflower.entity.enums.EntityType;
import com.kudr9tov.euphoriaflower.entity.enums.ResourceType;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.mappers.ResourceMapper;
import com.kudr9tov.euphoriaflower.persistence.InterfaceDAO;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class ProductDAO implements InterfaceDAO<Product> {
    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    private final CategoryDAO categoryDAO;
    private final ResourceDAO resourceDAO;
    private final AttributeValueDAO attributeValueDAO;

    @Autowired
    public ProductDAO(final JdbcTemplate jdbcTemplate,
                      final CategoryDAO categoryDAO,
                      final ResourceDAO resourceDAO,
                      final AttributeValueDAO attributeValueDAO) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
        this.categoryDAO = categoryDAO;
        this.resourceDAO = resourceDAO;
        this.attributeValueDAO = attributeValueDAO;
    }

    @Override
    public Page<Product> getAll(int page, int limit) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("offset", page > 1 ? page * limit : 0);
        params.put("limit", limit);
        List<Product> products = nameJdbcTemplate.query("SELECT p.id, p.name, p.description, p.short_description, p.price, p.sale_price, p.discount, p.slug, p.stock, p.rating, p.top, p.featured, p.is_new " +
                "FROM product p " +
                "OFFSET :offset " +
                "LIMIT :limit", params, mapResult());

        int totalCategories = elementsCount();

        Page<Product> productPage = new Page<>(page, limit);
        productPage.setContent(products);
        productPage.setTotalElements(totalCategories);
        productPage.setPages((int) Math.ceil((double) totalCategories / limit));
        return productPage;
    }

    @Override
    public Product get(Integer id) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        return nameJdbcTemplate.query("SELECT p.id, p.name, p.description, p.short_description, p.price, p.sale_price, p.discount, p.slug, p.stock, p.rating, p.top, p.featured, p.is_new " +
                        "FROM product p " +
                        "WHERE id = :id;", params, mapResult())
                .stream()
                .findFirst()
                .orElseThrow(() -> new NotFoundException(String.format("Product with id `%d` not found", id)));
    }

    @Override
    public Product create(Product product) {
        Map<String, Object> productParams = createParameters(product);
        return nameJdbcTemplate.query("INSERT INTO product (name, description, short_description, price, sale_price, discount, slug, stock, rating, top, featured, is_new) " +
                        "values (:name, :description, :shortDescription, :price, :salePrice, :discount, :slug, :stock, :rating, :top, :featured, :isNew) " +
                        "RETURNING id, name, description, short_description, price, sale_price, discount, slug, stock, rating, top, featured, is_new;", productParams, mapResultOnlyProduct())
                .stream()
                .findFirst()
                .orElseThrow();
    }

    @Override
    public Product update(Product product) {
        Map<String, Object> params = createParameters(product);
        return nameJdbcTemplate.query("UPDATE product " +
                        "SET name              = data.name, " +
                        "    description       = data.description, " +
                        "    short_description = data.short_description, " +
                        "    price             = data.price, " +
                        "    sale_price        = data.sale_price, " +
                        "    discount          = data.discount, " +
                        "    slug              = data.slug, " +
                        "    stock             = data.stock, " +
                        "    rating            = data.rating, " +
                        "    top               = data.top, " +
                        "    featured          = data.featured, " +
                        "    is_new            = data.is_new " +
                        "FROM (VALUES " +
                        "             (:name, :description, :shortDescription, :price, :salePrice, :discount, :slug, :stock, :rating, :top, :featured, :is_new) " +
                        ") data (name, description, short_description, price, sale_price, discount, slug, stock, rating, top, featured, is_new) " +
                        "WHERE id = :id " +
                        "RETURNING product.name, product.description, product.short_description, product.price, product.sale_price, product.discount, product.slug, product.stock, product.rating, product.top, product.featured, product.is_new", params, mapResult())
                .stream()
                .findFirst()
                .orElseThrow();
    }

    @Override
    public void delete(Integer id) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        nameJdbcTemplate.update("DELETE FROM product WHERE id = :id;", params);
    }

    @Override
    public boolean isExist(Integer id) {
        Boolean isExist = nameJdbcTemplate.queryForObject("SELECT exists(SELECT id FROM product WHERE id = :id);", ImmutableMap.of("id", id), Boolean.class);
        return ObjectUtils.defaultIfNull(isExist, Boolean.FALSE);
    }

    @Override
    public int elementsCount() {
        Integer count = nameJdbcTemplate.queryForObject("SELECT count(id) FROM product", Collections.emptyMap(), Integer.class);
        return ObjectUtils.defaultIfNull(count, 0);
    }


    public Boolean checkSlug(String slug) {
        return nameJdbcTemplate.queryForObject("SELECT exists(SELECT true FROM product WHERE slug = :slug);", ImmutableMap.of("slug", slug), Boolean.class);
    }

    public Product getProduct(String slug) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("slug", slug);
        return nameJdbcTemplate.query("SELECT p.id, p.name, p.description, p.short_description, p.price, p.sale_price, p.discount, p.slug, p.stock, p.rating, p.top, p.featured, p.is_new " +
                        "FROM product p " +
                        "WHERE p.slug = :slug;", params, mapResult())
                .stream()
                .findFirst()
                .orElseThrow(() -> new NotFoundException(String.format("Product with slug `%s` not found", slug)));
    }

    private Map<String, Object> createParameters(Product product) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("name", product.getName());
        params.put("description", product.getDescription());
        params.put("shortDescription", product.getShortDescription());
        params.put("price", product.getPrice());
        params.put("salePrice", product.getSalePrice());
        params.put("discount", product.getDiscount());
        params.put("slug", product.getSlug());
        params.put("stock", product.getStock());
        params.put("rating", product.getRating());
        params.put("top", product.isTop());
        params.put("featured", product.isFeatured());
        params.put("isNew", product.isNew());
        return params;
    }

    private RowMapper<Product> mapResultOnlyProduct() {
        return (rs, i) -> mapProduct(rs);
    }

    private RowMapper<Product> mapResult() {
        return (rs, i) -> {
            Product product = mapProduct(rs);
            product.setCategories(categoryDAO.getCategoriesByProduct(product.getId()));
            List<Picture> pictures = resourceDAO.getResourcesByEntityAndEntityTypeAndResourceType(product.getId(), EntityType.PRODUCT, ResourceType.PICTURE)
                    .stream()
                    .map(ResourceMapper.resourceToPicture)
                    .collect(Collectors.toList());
            product.setPictures(pictures);
            List<AttributeValue> attributes = attributeValueDAO.getAttributesByProduct(product.getId());
            product.setAttributes(attributes);
            return product;
        };
    }

    private Product mapProduct(ResultSet rs) throws SQLException {
        Product product = new Product();
        product.setId(rs.getInt(1));
        product.setName(rs.getString(2));
        product.setDescription(rs.getString(3));
        product.setShortDescription(rs.getString(4));
        product.setPrice(rs.getDouble(5));
        product.setSalePrice(rs.getDouble(6));
        product.setDiscount(rs.getInt(7));
        product.setSlug(rs.getString(8));
        product.setStock(rs.getInt(9));
        product.setRating(rs.getDouble(10));
        product.setTop(rs.getBoolean(11));
        product.setFeatured(rs.getBoolean(12));
        product.setNew(rs.getBoolean(13));
        return product;
    }
}
