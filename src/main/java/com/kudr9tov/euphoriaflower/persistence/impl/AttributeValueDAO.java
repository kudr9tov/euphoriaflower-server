package com.kudr9tov.euphoriaflower.persistence.impl;

import com.google.common.collect.Maps;
import com.kudr9tov.euphoriaflower.entity.Attribute;
import com.kudr9tov.euphoriaflower.entity.AttributeValue;
import com.kudr9tov.euphoriaflower.entity.AttributeValues;
import com.kudr9tov.euphoriaflower.utils.JsonUtils;
import com.kudr9tov.euphoriaflower.utils.SymbolUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class AttributeValueDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(AttributeValueDAO.class);

    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public AttributeValueDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }


    public AttributeValue addAttributeValue(AttributeValue attributeValue) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("id", attributeValue.getId());
        params.put("name", attributeValue.getName());
        params.put("productId", attributeValue.getProductId());
        params.put("value", attributeValue.getValue());
        return nameJdbcTemplate.query("WITH add as( " +
                        "    INSERT INTO product_attribute_value (attribute_id, product_id, value) " +
                        "    VALUES (:attributeId, :productId, :value) " +
                        "    returning attribute_id, product_id, value " +
                        ") " +
                        "SELECT attr.id, attr.name, add.product_id, add.value " +
                        "FROM attribute attr, add " +
                        "WHERE add.attribute_id = attr.id;", params, mapAttributeValue())
                .stream()
                .findFirst()
                .orElseThrow();
    }

    public List<AttributeValue> getAttributesByProduct(Integer productId) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("productId", productId);
        return nameJdbcTemplate.query("SELECT attr.id, attr.name, pav.product_id, pav.value " +
                "FROM product_attribute_value pav " +
                " INNER JOIN attribute attr on attr.id = pav.attribute_id " +
                "WHERE pav.product_id = :productId;", params, mapAttributeValue());
    }

    public List<AttributeValues> getAttributeValuesMap(Collection<Attribute> attributes) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("ids", StringUtils.defaultString(attributes.stream().map(Attribute::getId).filter(Objects::nonNull).map(String::valueOf).collect(Collectors.joining(SymbolUtils.COMMA))));
        params.put("names", StringUtils.defaultString(attributes.stream().map(Attribute::getName).filter(Objects::nonNull).collect(Collectors.joining(SymbolUtils.COMMA))));

        return nameJdbcTemplate.query("SELECT attr.id, attr.name, jsonb_agg(DISTINCT pav.value ORDER BY pav.value desc) " +
                        "FROM product_attribute_value pav " +
                        "  INNER JOIN attribute attr on attr.id = pav.attribute_id " +
                        "WHERE IIF(:ids > '', attr.id::TEXT = ANY (string_to_array(:ids, ',')), TRUE) " +
                        "  AND IIF(:names > '', attr.name = ANY (string_to_array(:names, ',')), TRUE) " +
                        "GROUP BY 1, 2;", params, (rs, rowNum) -> {

                    try {
                        Attribute attribute = new Attribute(rs.getInt(1), rs.getString(2));
                        Set<String> values = (Set<String>) JsonUtils.getInstance().jsonArrayToCollection(rs.getString(3), Set.class, String.class);
                        AttributeValues attributeValues = new AttributeValues();
                        attributeValues.setAttribute(attribute);
                        attributeValues.setValues(values);
                        return attributeValues;
                    } catch (IOException e) {
                        LOGGER.warn("Can't parse attribute values", e);
                    }
                    return null;
                })
                .stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }


    private RowMapper<AttributeValue> mapAttributeValue() {
        return (rs, rowNum) -> {
            AttributeValue result = new AttributeValue();
            result.setId(rs.getInt(1));
            result.setName(rs.getString(2));
            result.setProductId(rs.getInt(3));
            result.setValue(rs.getString(4));
            return result;
        };
    }
}
