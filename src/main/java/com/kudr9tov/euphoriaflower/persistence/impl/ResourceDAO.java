package com.kudr9tov.euphoriaflower.persistence.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.kudr9tov.euphoriaflower.entity.Page;
import com.kudr9tov.euphoriaflower.entity.Resource;
import com.kudr9tov.euphoriaflower.entity.enums.EntityType;
import com.kudr9tov.euphoriaflower.entity.enums.ResourceType;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.persistence.InterfaceDAO;
import com.kudr9tov.euphoriaflower.utils.JsonUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Map;

@Repository
public class ResourceDAO implements InterfaceDAO<Resource> {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceDAO.class);
    private final NamedParameterJdbcTemplate nameJdbcTemplate;

    @Autowired
    public ResourceDAO(final JdbcTemplate jdbcTemplate) {
        this.nameJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
    }

    @Override
    public Page<Resource> getAll(int page, int limit) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("offset", page > 1 ? page * limit : 0);
        params.put("limit", limit);
        List<Resource> resources = nameJdbcTemplate.query("SELECT id, resource_url, resource_parameters, type FROM resource OFFSET :offset LIMIT :limit", params, mapResult());
        int totalElements = elementsCount();

        Page<Resource> resourcePage = new Page<>(page, limit);
        resourcePage.setContent(resources);
        resourcePage.setTotalElements(totalElements);
        resourcePage.setPages((int) Math.ceil((double) totalElements / limit));
        return resourcePage;
    }

    @Override
    public Resource get(Integer id) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("id", id);
        return nameJdbcTemplate.query("SELECT id, resource_url, resource_parameters, type FROM resource WHERE id = :id", params, mapResult())
                .stream()
                .findFirst()
                .orElseThrow(() -> new NotFoundException(String.format("Resource with id `%d` not found", id)));
    }

    @Override
    public Resource create(Resource resource) {
        try {
            Map<String, Object> params = createParameters(resource);
            return nameJdbcTemplate.query("INSERT INTO resource (resource_url, resource_parameters, type) " +
                            "VALUES (:resourceUrl, cast(:resourceParameters AS JSONB), :type) " +
                            "RETURNING id, resource_url, resource_parameters, type;", params, mapResult())
                    .stream()
                    .findFirst()
                    .orElseThrow();
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getMessage(), e);
        }
        throw new IllegalStateException("Resource didn't save");
    }

    @Override
    public Resource update(Resource resource) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(Integer id) {
        nameJdbcTemplate.update("DELETE FROM resource WHERE id = :id", ImmutableMap.of("id", id));
    }

    @Override
    public boolean isExist(Integer id) {
        Boolean isExist = nameJdbcTemplate.queryForObject("SELECT exists(SELECT id FROM resource WHERE id = :id);", ImmutableMap.of("id", id), Boolean.class);
        return ObjectUtils.defaultIfNull(isExist, Boolean.FALSE);
    }

    public int elementsCount() {
        Integer count = nameJdbcTemplate.queryForObject("SELECT count(id) FROM resource", Collections.emptyMap(), Integer.class);
        return ObjectUtils.defaultIfNull(count, 0);
    }

    public List<Resource> getResourcesByEntityAndEntityTypeAndResourceType(Integer entityId, EntityType entityType, ResourceType type) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("entityId", entityId);
        params.put("entityType", entityType.name());
        params.put("type", type.name());

        return nameJdbcTemplate.query("SELECT r.id, r.resource_url, r.resource_parameters, r.type " +
                "FROM resource_to_entity rte " +
                "  INNER JOIN public.resource r on r.id = rte.resource_id " +
                "WHERE entity_id = :entityId  " +
                "  AND entity_type = :entityType  " +
                "  AND type = :type;", params, mapResult());
    }

    private RowMapper<Resource> mapResult() {
        return (rs, i) -> {
            Resource resource = new Resource();
            resource.setId(rs.getInt(1));
            resource.setResourceUrl(rs.getString(2));
            try {
                resource.setResourceParameters(JsonUtils.getInstance().jsonToMap(rs.getString(3)));
            } catch (JsonProcessingException e) {
                LOGGER.warn("Resource parameters didn't parse");
            }
            resource.setType(EnumUtils.getEnum(ResourceType.class, rs.getString(4)));
            return resource;
        };
    }

    public Resource addResourceToEntity(Integer resourceId, Integer entityId, EntityType entityType) {
        try {
            Map<String, Object> params = Maps.newHashMap();
            params.put("resourceId", resourceId);
            params.put("entityId", entityId);
            params.put("entityType", entityType.name());
            return nameJdbcTemplate.query("WITH add as ( " +
                            "    INSERT INTO resource_to_entity (resource_id, entity_id, entity_type)  " +
                            "    VALUES (:resourceId, :entityId, :entityType) " +
                            "    RETURNING resource_id " +
                            ") " +
                            "SELECT r.id, r.resource_url, r.resource_parameters, r.type " +
                            "FROM add, resource r " +
                            "WHERE r.id = add.resource_id", params, mapResult())
                    .stream()
                    .findFirst()
                    .orElseThrow();
        } catch (Exception e) {
            throw new IllegalStateException("Resource doesn't been added", e);
        }
    }

    private Map<String, Object> createParameters(Resource product) throws JsonProcessingException {
        Map<String, Object> params = Maps.newHashMap();
        params.put("resourceUrl", product.getResourceUrl());
        params.put("resourceParameters", JsonUtils.MAPPER.writeValueAsString(product.getResourceParameters()));
        params.put("type", product.getType().name());
        return params;
    }

    public void deleteEntityResources(Integer entityId, EntityType entityType) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("entityId", entityId);
        params.put("entityType", entityType.name());
        nameJdbcTemplate.update("DELETE FROM resource_to_entity WHERE entity_id = :entityId AND entity_type = :entityType;", params);
    }
}
