package com.kudr9tov.euphoriaflower.entity;

import com.google.common.collect.Sets;
import org.apache.commons.lang3.ObjectUtils;

import java.util.Collection;
import java.util.Set;

public class Product {
    private Integer id;
    private String name;
    private Double price;
    private Double salePrice;
    private Integer discount;

    private String description;
    private String shortDescription;

    private Set<Picture> pictures = Sets.newLinkedHashSet();
    private Set<Category> categories = Sets.newLinkedHashSet();
    private Set<AttributeValue> attributes = Sets.newLinkedHashSet();
    private String slug; //seo url

    private int stock;

    private Double rating;
    private boolean top = Boolean.FALSE;
    private boolean featured = Boolean.FALSE;
    private boolean isNew = Boolean.TRUE;

//    review?: number;
//    until?: string;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(Double salePrice) {
        this.salePrice = salePrice;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public Set<Picture> getPictures() {
        return ObjectUtils.defaultIfNull(pictures, Sets.newHashSet());
    }

    public void addPicture(Picture picture){
        this.pictures.add(picture);
    }

    public void setPictures(Collection<Picture> pictures) {
        this.pictures = Sets.newHashSet(pictures);
    }

    public Set<Category> getCategories() {
        return ObjectUtils.defaultIfNull(categories, Sets.newHashSet());
    }

    public void addCategory(Category category){
        this.categories.add(category);
    }

    public void setCategories(Collection<Category> categories) {
        this.categories = Sets.newHashSet(categories);
    }

    public Set<AttributeValue> getAttributes() {
        return attributes;
    }

    public void addAttribute(AttributeValue attributeValue){
        this.attributes.add(attributeValue);
    }

    public void setAttributes(Collection<AttributeValue> attributes) {
        this.attributes = Sets.newHashSet(attributes);
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public Double getRating() {
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public boolean isTop() {
        return top;
    }

    public void setTop(boolean top) {
        this.top = top;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }
}
