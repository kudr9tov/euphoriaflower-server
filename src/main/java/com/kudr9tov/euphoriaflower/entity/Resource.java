package com.kudr9tov.euphoriaflower.entity;

import com.google.common.collect.Maps;
import com.kudr9tov.euphoriaflower.entity.enums.ResourceType;

import java.util.Map;

public class Resource {
    private Integer id;
    private String resourceUrl;
    private Map<String, Object> resourceParameters = Maps.newLinkedHashMap();
    private ResourceType type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }

    public Map<String, Object> getResourceParameters() {
        return resourceParameters;
    }

    public void setResourceParameters(Map<String, Object> resourceParameters) {
        this.resourceParameters = resourceParameters;
    }

    public void addResourceParameter(String key, Object value) {
        this.resourceParameters.put(key, value);
    }

    public ResourceType getType() {
        return type;
    }

    public void setType(ResourceType type) {
        this.type = type;
    }

}
