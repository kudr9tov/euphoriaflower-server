package com.kudr9tov.euphoriaflower.entity;

import java.util.Objects;

public class AttributeValue extends Attribute {
    private Integer productId;
    private String value;

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AttributeValue)) return false;
        if (!super.equals(o)) return false;
        AttributeValue that = (AttributeValue) o;
        return Objects.equals(productId, that.productId) && Objects.equals(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), productId, value);
    }
}
