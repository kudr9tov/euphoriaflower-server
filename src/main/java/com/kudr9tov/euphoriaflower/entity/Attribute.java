package com.kudr9tov.euphoriaflower.entity;

import java.util.Objects;

public class Attribute {
    private Integer id;
    private String name;

    public Attribute() {

    }

    public Attribute(String name) {
        this.name = name;
    }

    public Attribute(Integer id) {
        this.id = id;
    }

    public Attribute(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Attribute attribute = (Attribute) o;
        return Objects.equals(id, attribute.id) && Objects.equals(name, attribute.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
