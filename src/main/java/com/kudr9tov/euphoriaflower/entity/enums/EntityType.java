package com.kudr9tov.euphoriaflower.entity.enums;

public enum EntityType {
    BANNER, PRODUCT, CATEGORY
}
