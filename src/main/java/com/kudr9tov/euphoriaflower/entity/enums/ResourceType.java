package com.kudr9tov.euphoriaflower.entity.enums;

public enum ResourceType {
    PICTURE, FILE
}
