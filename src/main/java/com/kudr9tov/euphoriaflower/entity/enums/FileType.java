package com.kudr9tov.euphoriaflower.entity.enums;

public enum FileType {
    XLSX(ResourceType.FILE),
    DOC(ResourceType.FILE),
    WEBP(ResourceType.PICTURE),
    JPG(ResourceType.PICTURE),
    PNG(ResourceType.PICTURE);

    private final ResourceType resourceType;

    FileType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    public ResourceType getResourceType() {
        return resourceType;
    }
}
