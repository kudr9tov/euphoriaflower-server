package com.kudr9tov.euphoriaflower.entity;

import java.util.Set;

public class AttributeValues {
    private Attribute attribute;
    private Set<String> values;

    public Attribute getAttribute() {
        return attribute;
    }

    public void setAttribute(Attribute attribute) {
        this.attribute = attribute;
    }

    public Set<String> getValues() {
        return values;
    }

    public void setValues(Set<String> values) {
        this.values = values;
    }
}
