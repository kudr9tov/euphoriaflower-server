package com.kudr9tov.euphoriaflower.controller;

import com.kudr9tov.euphoriaflower.entity.Attribute;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.service.impl.AttributeService;
import com.kudr9tov.euphoriaflower.utils.APIConstants;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

/*
 *
 * GET     /api/attribute/                  - get page of attribute
 * POST    /api/attribute/                  - create attribute
 * PUT     /api/attribute/                  - update attribute
 * DELETE  /api/attribute/{id}              - delete attribute by id
 * GET     /api/attribute/filter-values     - get values of attribute, grouped by attribute
 *
 */
@RestController
@RequestMapping(value = APIConstants.ATTRIBUTE, consumes = {"*/*", "application/json"}, produces = {"*/*", "application/json"})
public class AttributeController {

    private final AttributeService service;

    public AttributeController(AttributeService service) {
        this.service = service;
    }

    @GetMapping(value = "")
    public ResponseEntity<Object> getAllAttributes(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                   @RequestParam(value = "limit", defaultValue = "12") Integer limit) {
        return ResponseEntity.ok().body(service.getAll(page, limit));
    }

    @PostMapping(value = "")
    public ResponseEntity<Object> createAttribute(@RequestBody Attribute attribute) {
        return ResponseEntity.ok().body(service.create(attribute));
    }

    @PutMapping(value = "")
    public ResponseEntity<Object> updateAttribute(@RequestBody Attribute attribute) {
        return ResponseEntity.ok().body(service.update(attribute));
    }

    @DeleteMapping(value = "/{id}")
    public void deleteAttribute(@PathVariable Integer id) {
        service.delete(id);
    }

    @PostMapping(value = "filter-values")
    public ResponseEntity<Object> getFilterValues(@RequestParam(value = "attributeIds", required = false) Set<Integer> attributeIds,
                                                  @RequestParam(value = "attributeNames", required = false) Set<String> attributeNames) {
        if (CollectionUtils.isNotEmpty(attributeIds)) {
            return ResponseEntity.ok().body(service.getAttributeValues(attributeIds.stream().map(Attribute::new).collect(Collectors.toList())));
        }
        if (CollectionUtils.isNotEmpty(attributeNames)) {
            return ResponseEntity.ok().body(service.getAttributeValues(attributeNames.stream().map(Attribute::new).collect(Collectors.toList())));
        }
        return ResponseEntity.noContent().build();
    }

}
