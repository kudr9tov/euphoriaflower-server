package com.kudr9tov.euphoriaflower.controller;

import com.kudr9tov.euphoriaflower.entity.Banner;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.service.impl.BannerService;
import com.kudr9tov.euphoriaflower.utils.APIConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 *
 * GET    /api/banner/            - get page of banner
 * GET    /api/banner/{id}        - get banner by id
 * POST   /api/banner/            - create banner
 * PUT    /api/banner/            - update banner
 * DELETE /api/banner/{id}        - delete banner by id
 *
 */
@RestController
@RequestMapping(value = APIConstants.BANNER, consumes = {"*/*", "application/json"}, produces = {"*/*", "application/json"})
public class BannerController {

    private final BannerService service;

    public BannerController(BannerService service) {
        this.service = service;
    }

    @GetMapping(value = "")
    public ResponseEntity<Object> getAllBanners(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                @RequestParam(value = "limit", defaultValue = "12") Integer limit) {
        return ResponseEntity.ok().body(service.getAll(page, limit));
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<Object> getBanner(@PathVariable Integer id) {
        return ResponseEntity.ok().body(service.get(id));
    }

    @PostMapping(value = "")
    public ResponseEntity<Object> createBanner(@RequestBody Banner banner) {
        return ResponseEntity.ok().body(service.create(banner));
    }

    @PutMapping(value = "")
    public ResponseEntity<Object> updateBanner(@RequestBody Banner banner) {
        return ResponseEntity.ok().body(service.update(banner));
    }

    @DeleteMapping(value = "/{id}")
    public void deleteBanner(@PathVariable Integer id) {
        service.delete(id);
    }

}
