package com.kudr9tov.euphoriaflower.controller;

import com.kudr9tov.euphoriaflower.entity.Product;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.service.impl.ProductService;
import com.kudr9tov.euphoriaflower.utils.APIConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 *
 * GET    /api/product/            - get page of products
 * GET    /api/product/{slug}      - get product by slug(seo url)
 * POST   /api/product/            - create product
 * PUT    /api/product/            - update product
 * DELETE /api/product/{id}        - delete product by id
 *
 */
@RestController
@RequestMapping(value = APIConstants.PRODUCT, consumes = {"*/*", "application/json"}, produces = {"*/*", "application/json"})
public class ProductController {

    private final ProductService service;

    public ProductController(ProductService service) {
        this.service = service;
    }

    @GetMapping(value = "")
    public ResponseEntity<Object> getAllProducts(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                 @RequestParam(value = "limit", defaultValue = "12") Integer limit) {
        return ResponseEntity.ok().body(service.getAll(page, limit));
    }

    @GetMapping(value = "/{slug}")
    public ResponseEntity<Object> getProduct(@PathVariable String slug) {
        return ResponseEntity.ok().body(service.getProduct(slug));
    }

    @PostMapping(value = "")
    public ResponseEntity<Object> createProduct(@RequestBody Product banner) {
        return ResponseEntity.ok().body(service.create(banner));
    }

    @PutMapping(value = "")
    public ResponseEntity<Object> updateProduct(@RequestBody Product banner) {
        return ResponseEntity.ok().body(service.update(banner));
    }

    @DeleteMapping(value = "/{id}")
    public void deleteProduct(@PathVariable Integer id) {
        service.delete(id);
    }

}
