package com.kudr9tov.euphoriaflower.controller;

import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.service.impl.ResourceService;
import com.kudr9tov.euphoriaflower.utils.APIConstants;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/*
 *
 * GET    /api/resource/{file}      - get InputStream file
 * GET    /api/resource/            - get page of resources
 * GET    /api/resource/entity/{id} - get resource by id
 * POST   /api/resource/            - create resource from MultipartFile
 * DELETE /api/resource/{id}        - delete resource by id
 *
 */
@RestController
@RequestMapping(value = APIConstants.RESOURCE, consumes = {"*/*", "application/json"}, produces = {"*/*", "application/json"})
public class ResourceController {

    private final ResourceService service;

    public ResourceController(ResourceService service) {
        this.service = service;
    }

    @GetMapping(value = "/{file}", produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.APPLICATION_PDF_VALUE, MediaType.ALL_VALUE})
    public @ResponseBody
    byte[] getImage(@PathVariable String file) throws IOException {

        File resource = service.getResource(file);
        InputStream in = new FileInputStream(resource);
        return IOUtils.toByteArray(in);
    }

    @GetMapping(value = "")
    public ResponseEntity<Object> getAllResources(@RequestParam(value = "page", defaultValue = "0") Integer page,
                                                  @RequestParam(value = "limit", defaultValue = "12") Integer limit) {
        return ResponseEntity.ok().body(service.getAll(page, limit));
    }

    @GetMapping(value = "/entity/{id}")
    public ResponseEntity<Object> getResource(@PathVariable Integer id) {
        return ResponseEntity.ok().body(service.get(id));
    }

    @PostMapping(value = "", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Object> uploadFiles(@RequestParam("file") MultipartFile file) {
        return ResponseEntity.ok().body(service.createResource(file));
    }

    @DeleteMapping(value = "/{id}")
    public void deleteResource(@PathVariable Integer id) {
        service.delete(id);
    }

}
