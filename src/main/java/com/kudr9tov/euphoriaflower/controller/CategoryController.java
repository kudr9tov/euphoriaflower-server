package com.kudr9tov.euphoriaflower.controller;

import com.kudr9tov.euphoriaflower.entity.Category;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.service.impl.CategoryService;
import com.kudr9tov.euphoriaflower.utils.APIConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/*
 *
 * GET    /api/category/            - get page of category
 * GET    /api/category/{slug}      - get category by slug(seo url)
 * POST   /api/category/            - create category
 * PUT    /api/category/            - update category
 * DELETE /api/category/{id}        - delete category by id
 *
 */
@RestController
@RequestMapping(value = APIConstants.CATEGORY, consumes = {"*/*", "application/json"}, produces = {"*/*", "application/json"})
public class CategoryController {

    private final CategoryService service;

    public CategoryController(CategoryService service) {
        this.service = service;
    }

    @GetMapping(value = "")
    public ResponseEntity<Object> getAllCategories(@RequestParam(value = "page", defaultValue = "1") Integer page,
                                                   @RequestParam(value = "limit", defaultValue = "12") Integer limit) {
        return ResponseEntity.ok().body(service.getAll(page, limit));
    }

    @GetMapping(value = "/{slug}")
    public ResponseEntity<Object> getCategory(@PathVariable String slug) {
        return ResponseEntity.ok().body(service.getCategory(slug));
    }

    @PostMapping(value = "")
    public ResponseEntity<Object> createCategory(@RequestBody Category category) {
        return ResponseEntity.ok().body(service.create(category));
    }

    @PutMapping(value = "")
    public ResponseEntity<Object> updateCategory(@RequestBody Category category) {
        return ResponseEntity.ok().body(service.update(category));
    }

    @DeleteMapping(value = "/{id}")
    public void deleteCategory(@PathVariable Integer id) {
        service.delete(id);
    }

}
