package com.kudr9tov.euphoriaflower.controller.handler;

import com.kudr9tov.euphoriaflower.exception.AlreadyExistException;
import com.kudr9tov.euphoriaflower.exception.NotFoundException;
import com.kudr9tov.euphoriaflower.utils.model.ApiError;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.NoSuchElementException;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    protected ResponseEntity<Object> handleNotFoundException(
            NotFoundException ex,
            WebRequest webRequest) {
        HttpServletRequest request = ((ServletWebRequest) webRequest).getRequest();
        ApiError apiError = new ApiError(
                request.getMethod(),
                request.getRequestURI(),
                HttpStatus.NOT_FOUND,
                ex.getLocalizedMessage(),
                Collections.emptyList()
        );
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler(NoSuchElementException.class)
    protected ResponseEntity<Object> handleNotFoundException(
            NoSuchElementException ex,
            WebRequest webRequest) {
        HttpServletRequest request = ((ServletWebRequest) webRequest).getRequest();
        ApiError apiError = new ApiError(
                request.getMethod(),
                request.getRequestURI(),
                HttpStatus.NOT_FOUND,
                ex.getLocalizedMessage(),
                Collections.emptyList()
        );
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler(AlreadyExistException.class)
    protected ResponseEntity<Object> handleNotFoundException(
            AlreadyExistException ex,
            WebRequest webRequest) {
        HttpServletRequest request = ((ServletWebRequest) webRequest).getRequest();
        ApiError apiError = new ApiError(
                request.getMethod(),
                request.getRequestURI(),
                HttpStatus.BAD_REQUEST,
                ex.getLocalizedMessage(),
                Collections.emptyList()
        );
        return new ResponseEntity<>(apiError, new HttpHeaders(), apiError.getStatus());
    }
}
