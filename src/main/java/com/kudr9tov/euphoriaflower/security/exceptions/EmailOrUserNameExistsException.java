package com.kudr9tov.euphoriaflower.security.exceptions;

public class EmailOrUserNameExistsException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public EmailOrUserNameExistsException(String email) {
		super(String.format("Email exists: email=%s", email));
	}

}
