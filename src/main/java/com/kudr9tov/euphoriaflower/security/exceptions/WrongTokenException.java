package com.kudr9tov.euphoriaflower.security.exceptions;

public class WrongTokenException extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public WrongTokenException() {
		super();
	}

}
