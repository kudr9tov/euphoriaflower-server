package com.kudr9tov.euphoriaflower.security.endpoints.dto;


public class ForgotPasswordDTO {
	private String email;

	public String getEmail() {
		return email;
	}
}
