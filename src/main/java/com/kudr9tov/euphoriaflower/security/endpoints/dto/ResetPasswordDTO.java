package com.kudr9tov.euphoriaflower.security.endpoints.dto;

public class ResetPasswordDTO {

	private String resetId;
	private String newPassword;

	public String getResetId() {
		return resetId;
	}

	public void setResetId(String resetId) {
		this.resetId = resetId;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

}
