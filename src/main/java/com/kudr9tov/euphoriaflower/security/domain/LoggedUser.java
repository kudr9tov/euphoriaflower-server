package com.kudr9tov.euphoriaflower.security.domain;


import com.kudr9tov.euphoriaflower.security.persistence.entities.UserEntity;
import com.kudr9tov.euphoriaflower.security.persistence.entities.UserRoles;
import com.kudr9tov.euphoriaflower.security.persistence.entities.UserStatuses;

public class LoggedUser {

	private String id;
	private String email;
	private String name;
	private UserStatuses status;
	private UserRoles role;
	
	public LoggedUser(UserEntity entity) {
		super();
		this.id = entity.getId();
		this.email = entity.getEmail();
		this.name = entity.getUserName();
		this.status = entity.getUserStatus();
		this.role = entity.getUserRole();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public UserStatuses getStatus() {
		return status;
	}

	public void setStatus(UserStatuses status) {
		this.status = status;
	}

	public UserRoles getRole() {
		return role;
	}

	public void setRole(UserRoles role) {
		this.role = role;
	}
}
