package com.kudr9tov.euphoriaflower.security.filters;

import com.kudr9tov.euphoriaflower.security.services.TokenAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthLogoutFilter extends GenericFilterBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthLogoutFilter.class);

    private final RequestMatcher requestMatcher;
    private final TokenAuthenticationService authenticationService;

    public AuthLogoutFilter(String url, TokenAuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
        requestMatcher = new AntPathRequestMatcher(url, HttpMethod.GET.toString());
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        if (requestMatcher.matches((HttpServletRequest) request)) {
            ((HttpServletResponse) response).addCookie(authenticationService.deleteAccessCookie());
        } else {
            chain.doFilter(request, response);
        }
    }

}
