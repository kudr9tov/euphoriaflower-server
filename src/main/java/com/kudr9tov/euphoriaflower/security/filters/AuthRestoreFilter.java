package com.kudr9tov.euphoriaflower.security.filters;

import com.kudr9tov.euphoriaflower.security.Tokens;
import com.kudr9tov.euphoriaflower.security.domain.LoggedUser;
import com.kudr9tov.euphoriaflower.security.persistence.entities.UserEntity;
import com.kudr9tov.euphoriaflower.security.persistence.repositories.JPAUserRepository;
import com.kudr9tov.euphoriaflower.security.serializers.LoginUserDTOSerializer;
import com.kudr9tov.euphoriaflower.security.services.TokenAuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

public class AuthRestoreFilter extends GenericFilterBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthRestoreFilter.class);

    private final TokenAuthenticationService authenticationService;
    private final JPAUserRepository userRepo;
    private final RequestMatcher requestMatcher;

    public AuthRestoreFilter(String url, TokenAuthenticationService authenticationService, JPAUserRepository userRepo) {
        this.authenticationService = authenticationService;
        this.userRepo = userRepo;
        requestMatcher = new AntPathRequestMatcher(url, HttpMethod.GET.toString());
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        if (requestMatcher.matches((HttpServletRequest) request)) {
            try {
                final String userNameFromToken = authenticationService.getUserNameFromToken(request, Tokens.REFRESH_TOKEN);

                UserEntity entity = userRepo.findUserByEmail(userNameFromToken);
                if (Objects.nonNull(entity)) {
                    authenticationService.addAccessToken((HttpServletResponse) response, entity);
                    final LoginUserDTOSerializer serializer = new LoginUserDTOSerializer();
                    response.getWriter().write(serializer.serialize(new LoggedUser(entity)));
                } else {
                    ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                    ((HttpServletResponse) response).addCookie(authenticationService.deleteRefreshCookie());
                    response.getWriter().write("{\"message\":\"You was logout. Please sing up\"}");
                }
            } catch (Exception ignored) {
                ((HttpServletResponse) response).addCookie(authenticationService.deleteRefreshCookie());
                chain.doFilter(request, response);
            }
        } else {
            chain.doFilter(request, response);
        }
    }

}
