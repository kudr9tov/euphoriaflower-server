package com.kudr9tov.euphoriaflower.security.filters;

import com.kudr9tov.euphoriaflower.security.Tokens;
import com.kudr9tov.euphoriaflower.security.exceptions.WrongTokenException;
import com.kudr9tov.euphoriaflower.security.persistence.entities.UserEntity;
import com.kudr9tov.euphoriaflower.security.persistence.repositories.JPAUserRepository;
import com.kudr9tov.euphoriaflower.security.services.TokenAuthenticationService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JWTAuthenticationFilter extends GenericFilterBean {
    private static final Logger LOGGER = LoggerFactory.getLogger(JWTAuthenticationFilter.class);

    private final TokenAuthenticationService authenticationService;
    private final JPAUserRepository userRepo;

    public JWTAuthenticationFilter(TokenAuthenticationService authenticationService, JPAUserRepository userRepo) {
        this.authenticationService = authenticationService;
        this.userRepo = userRepo;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        try {
            String token = authenticationService.getAccessTokenFromRequest(request);
            if (token == null) {
                SecurityContextHolder.getContext().setAuthentication(null);
                filterChain.doFilter(request, response);
                return;
            }
            String email = authenticationService.getUserNameFromToken(request, Tokens.ACCESS_TOKEN);
            UserEntity user = userRepo.findUserByEmail(email);
            Authentication authentication = user != null && user.getEmail().equals(email)
                    ? authenticationService.getAccessAuthentication(request)
                    : null;
            SecurityContextHolder.getContext().setAuthentication(authentication);
            filterChain.doFilter(request, response);
        } catch (ExpiredJwtException e) {
            LOGGER.debug("JWT has been expired");
            ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            ((HttpServletResponse) response).addCookie(authenticationService.deleteAccessCookie());
            response.getWriter().write("{\"message\": \"TOKEN_EXPIRED:\"}");
        } catch (SignatureException | MalformedJwtException e) {
            LOGGER.debug("JWT has been broken");
            ((HttpServletResponse) response).addCookie(authenticationService.deleteAccessCookie());
        } catch (WrongTokenException e) {
            LOGGER.debug("JWT has been empty or with wrong type");
            ((HttpServletResponse) response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            ((HttpServletResponse) response).addCookie(authenticationService.deleteAccessCookie());
            response.getWriter().write("{\"message\": \"TOKEN_EXPIRED\"}");
        }
    }
}
