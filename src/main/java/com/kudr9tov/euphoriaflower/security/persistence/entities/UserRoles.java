package com.kudr9tov.euphoriaflower.security.persistence.entities;

public enum UserRoles {

	GUEST, USER, ADMIN, MANAGER

}
