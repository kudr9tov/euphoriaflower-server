package com.kudr9tov.euphoriaflower.security;

import com.google.common.hash.Hashing;
import com.kudr9tov.euphoriaflower.security.domain.AccountCredentials;
import com.kudr9tov.euphoriaflower.security.domain.UserData;
import com.kudr9tov.euphoriaflower.security.endpoints.dto.ModifyUserDTO;
import com.kudr9tov.euphoriaflower.security.endpoints.dto.ResetPasswordDTO;
import com.kudr9tov.euphoriaflower.security.endpoints.dto.UpdateUserDTO;
import com.kudr9tov.euphoriaflower.security.persistence.entities.UserEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

@Service
public interface UserService {

    UserEntity createNewUser(AccountCredentials userDto);

    void deleteUserById(String id);

    UserEntity updateUser(String id, UpdateUserDTO userDto);

    Collection<UserEntity> getAllUsers();

    UserEntity getUserById(String id);

    UserEntity getUserByEmail(String email);

    UserEntity getActiveUserByEmail(String email);

    UserData modifyUser(HttpServletRequest request, HttpServletResponse response, ModifyUserDTO passwordDto, String userId);

    void passwordToReset(String id);

    void resetPassword(ResetPasswordDTO rpDto);

    void unfollowMailing(String mail);

    static String currentUserId() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth == null ? null : auth.getName();
    }

    static String md5Hex(String data) {
        return Hashing.sha256().hashString(data, StandardCharsets.UTF_8).toString();
    }
}
