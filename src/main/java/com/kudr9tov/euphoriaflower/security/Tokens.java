package com.kudr9tov.euphoriaflower.security;

public enum Tokens {

	ACCESS_TOKEN("access", "auth-token"),
	REFRESH_TOKEN("access", "auth-refresh");

	private String type;
	private String cookieName;
	
	Tokens(String type, String cookieName) {
		this.type = type;
		this.cookieName = cookieName;
	}
	
	public String getType() {
		return this.type;
	}
	
	public String getCookieName() {
		return this.cookieName;
	}	
	
}
