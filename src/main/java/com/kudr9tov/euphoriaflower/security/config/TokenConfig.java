package com.kudr9tov.euphoriaflower.security.config;

import com.kudr9tov.euphoriaflower.security.persistence.entities.UserEntity;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class TokenConfig {
    @Value("${equipments.security.secret}")
    private String secret;

    public String buildToken(UserEntity entity, long expirationTime, String type) {
        return Jwts.builder().setId(entity.getId())
                .setSubject(entity.getEmail())
                .setExpiration(new Date(System.currentTimeMillis() + expirationTime))
                .setHeaderParam("role", entity.getUserRole().toString())
                .setHeaderParam("type", type)
                .signWith(SignatureAlgorithm.HS512, getSecret()).compact();
    }

    public long getAccessTokenExpirationTime() {
        return 3600000;
    }

    public long getRefreshTokenExpirationTime() {
        return 518400000;
    }

    public String getSecret() {
        return secret;
    }
}
