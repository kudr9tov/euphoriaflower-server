package com.kudr9tov.euphoriaflower.security.services;

import com.google.common.collect.Sets;
import com.kudr9tov.euphoriaflower.security.Tokens;
import com.kudr9tov.euphoriaflower.security.UserService;
import com.kudr9tov.euphoriaflower.security.config.TokenConfig;
import com.kudr9tov.euphoriaflower.security.exceptions.UserNotExistsException;
import com.kudr9tov.euphoriaflower.security.persistence.entities.UserEntity;
import com.kudr9tov.euphoriaflower.security.persistence.entities.UserStatuses;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@Service
public class TokenAuthenticationService {

    private final TokenConfig tokenConfig;
    private final UserService userService;

    @Autowired
    public TokenAuthenticationService(TokenConfig tokenConfig,
                                      UserService userService) {
        this.tokenConfig = tokenConfig;
        this.userService = userService;
    }


    public void addAccessToken(HttpServletResponse res, UserEntity entity) {
        final Cookie cookie = new Cookie(Tokens.ACCESS_TOKEN.getCookieName(), buildHeader(entity, tokenConfig.getAccessTokenExpirationTime(), Tokens.ACCESS_TOKEN.getType()));
        cookie.setMaxAge(24 * 60 * 60);
        cookie.setPath("/");
        res.addCookie(cookie);
    }

    public Authentication getAccessAuthentication(ServletRequest request) {
        final String token = getTokenFromCookie((HttpServletRequest) request, Tokens.ACCESS_TOKEN.getCookieName());
        return parseToken(token, Tokens.ACCESS_TOKEN.getType());
    }

    public void addRefreshToken(HttpServletResponse res, UserEntity entity) {
        final Cookie cookie = new Cookie(Tokens.REFRESH_TOKEN.getCookieName(), buildHeader(entity, tokenConfig.getRefreshTokenExpirationTime(), Tokens.REFRESH_TOKEN.getType()));
        cookie.setMaxAge(30 * 24 * 60 * 60);
        cookie.setPath("/");
        res.addCookie(cookie);
    }

    public Authentication getRefreshAuthentication(ServletRequest request) {
        final String token = getTokenFromCookie((HttpServletRequest) request, Tokens.REFRESH_TOKEN.getCookieName());
        return parseToken(token, Tokens.REFRESH_TOKEN.getType());
    }


    public String getUserNameFromToken(final ServletRequest request, Tokens reqToken) {
        final String token = Tokens.ACCESS_TOKEN.equals(reqToken) ? getAccessTokenFromRequest(request) : getRefreshTokenFromRequest(request);
        if (StringUtils.isNotBlank(token)) {
            return getClaims(token).getBody().getSubject();
        }
        return null;
    }

    public String getAccessTokenFromRequest(final ServletRequest request) {
        return getTokenFromCookie((HttpServletRequest) request, Tokens.ACCESS_TOKEN.getCookieName());
    }

    public String getRefreshTokenFromRequest(final ServletRequest request) {
        return getTokenFromCookie((HttpServletRequest) request, Tokens.REFRESH_TOKEN.getCookieName());
    }

    private String getTokenFromCookie(HttpServletRequest request, String cookieName) {
        final Cookie[] cookies = request.getCookies();
        if (ArrayUtils.isNotEmpty(cookies)) {
            final Optional<Cookie> auth = Stream.of(cookies)
                    .filter(f -> f.getName().equalsIgnoreCase(cookieName))
                    .findFirst();
            if (auth.isEmpty()) {
                return null;
            }
            final Cookie cookie = auth.get();
            return cookie.getValue();
        }
        return null;
    }

    private Authentication parseToken(String token, String type) {
        if (token != null && !token.trim().isEmpty()) {
            Jws<Claims> claim = getClaims(token);
            String userId = claim.getBody().getId();

            String role = claim.getHeader().get("role").toString();
            String tokenType = claim.getHeader().get("type").toString();
            return getAuthentication(type, userId, role, tokenType);
        }
        return null;
    }

    public Authentication getAuthentication(String type, String userId, String role, String tokenType) {
        try {
            UserEntity userEntity = userService.getUserById(userId);
            if (Objects.isNull(userEntity) || UserStatuses.getInActive().contains(userEntity.getUserStatus())) {
                return null;
            }

        } catch (UserNotExistsException ex) {
            return null;
        }
        Collection<GrantedAuthority> authorities = Sets.newHashSet();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + role));
        return userId != null && tokenType.equals(type) ? new UsernamePasswordAuthenticationToken(userId, null, authorities) : null;
    }

    private Jws<Claims> getClaims(final String token) {
        return Jwts.parser()
                .setSigningKey(tokenConfig.getSecret())
                .parseClaimsJws(token);
    }

    private String buildHeader(UserEntity entity, long expirationTime, String token) {
        return tokenConfig.buildToken(entity, expirationTime, token);
    }

    public Cookie deleteAccessCookie() {
        final Cookie cookie = new Cookie(Tokens.ACCESS_TOKEN.getCookieName(), null);
        cookie.setMaxAge(0);
        cookie.setPath("/");
        return cookie;
    }

    public Cookie deleteRefreshCookie() {
        final Cookie cookie = new Cookie(Tokens.REFRESH_TOKEN.getCookieName(), null);
        cookie.setMaxAge(0);
        cookie.setPath("/");
        return cookie;
    }
}
